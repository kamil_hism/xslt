<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">

  <html>
  <head>
    <script type="text/javascript" src="http://stickerjs.cmiscm.com/js/sticker.min.js"></script>
    <style type="text/css">
      @font-face {
        font-family: "Roboto-Light";
        src: url(Roboto-Light.ttf);
      }
      body {
        font-family: "Roboto-Light", sans-serif;
        color: #fff;
        background-color: #465b74;
        margin: 0;
        padding: 0;
        text-align: center;
      }
      li {
        position: relative;
        width: 220px;
        height: 220px;
        float: left;
        list-style: none;
        display: block;
        margin: 50px 60px;
      }
      .des {
        font-weight: 400;
        font-size: 12px;
        text-align: center;
        text-transform:uppercase;
      }
      ul {
        position: relative;
        margin: 10px auto 0 auto;
        width: 680px;
        height: 270px;
      }
      h3 {
        display: block;
        position: relative;
        font-size: 80px;
        font-weight: 100;
        line-height: 0px;
      }
      p a {
        color: #fff;
        text-decoration: underline;
        transition: color 0.2s linear;
        -webkit-transition: color 0.2s linear;
      }
      p {
      font: 14px/22px Helvetica,Arial,sans-serif;
      margin: 10px 0;
      }
    </style>
  </head>
  <body>
    <h3>Заповедники России</h3>
    <p>Источник фотографий: <a href="http://vk.com/wwf.russia" target="_blank">WWF</a></p>

    <ul>
      <xsl:for-each select="response/photo">
        <li class="sticker">

          <xsl:attribute name="class">sticker <xsl:value-of select='pid'/></xsl:attribute>
          <xsl:attribute name="title"><xsl:value-of select='text'/></xsl:attribute>

          <p class="des"><xsl:value-of select='substring(text, 0, 25)'/>...</p>
          <img style="display: none;">
              <xsl:attribute name="src">
                  <xsl:value-of select="src_big"/>
              </xsl:attribute>
          </img>
        </li>
      </xsl:for-each>
    </ul>

    <script type="text/javascript">
      window.onload = function(){
        Sticker.init('.sticker');
        var stickers = document.getElementsByClassName('sticker');
        var sticker = 0;
        while (sticker != stickers.length) {
          var stick = stickers[sticker];
          var pid = stick.getAttribute('class').split(' ')[1],
            psrc = stick.getElementsByTagName('img')[0].src;

          var elems = document.getElementsByClassName(pid)[0].getElementsByClassName('sticker-img');
          var count = 0;
          while(count != elems.length) {
             elems[count].style.backgroundImage = "url('" + psrc + "')";
             count +=1 ;
          }

          sticker += 1;
        }
      }
    </script>
  </body>
  </html>

</xsl:template>
</xsl:stylesheet>
